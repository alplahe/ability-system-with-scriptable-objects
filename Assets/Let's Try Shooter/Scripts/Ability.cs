﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public string m_aNull;
    public Sprite m_aSprite;
    public AudioClip m_aSound;
    public float m_aBaseCoolDown = 1f;

    public abstract void Initialize(GameObject gameObject);
    public abstract void TriggerAbility();
}
