﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShootTriggerable : MonoBehaviour
{
	[HideInInspector] public Rigidbody m_projectile;
	public Transform m_bulletSpawn;
	[HideInInspector] public float m_projectileForce = 250f;

	public void Launch()
	{
		Rigidbody clonedBullet = Instantiate(
			m_projectile, 
			m_bulletSpawn.position, 
			transform.rotation * Quaternion.Euler(90,0,0)) as Rigidbody;
		
		clonedBullet.AddForce(m_bulletSpawn.transform.forward * m_projectileForce);
	}
}
