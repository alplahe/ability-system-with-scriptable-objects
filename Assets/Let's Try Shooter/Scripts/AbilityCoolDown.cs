﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityCoolDown : MonoBehaviour
{
	public string m_abilityButtonAxisName = "Fire1";
	public Image m_darkMask;
	public Text m_coolDownTextDisplay;

	[SerializeField] private Ability m_ability;
	[SerializeField] private GameObject m_weaponHolder;
	private Image m_myButtonImage;
	private AudioSource m_abilitySource;
	private float m_coolDownDuration;
	private float m_nextReadyTime;
	private float m_coolDownTimeLeft;

	// Use this for initialization
	void Start ()
	{
		Initialize(m_ability, m_weaponHolder);
	}

	public void Initialize(Ability selectedAbility, GameObject weaponHolder)
	{
		m_ability = selectedAbility;
		m_myButtonImage = GetComponent<Image>();
		m_abilitySource = GetComponent<AudioSource>();
		m_myButtonImage.sprite = m_ability.m_aSprite;
		m_darkMask.sprite = m_ability.m_aSprite;
		m_coolDownDuration = m_ability.m_aBaseCoolDown;
		m_ability.Initialize(weaponHolder);
		AbilityReady();
	}

	// Update is called once per frame
	void Update ()
	{
		if (IsCoolDownComplete())
		{
			DoAbility();
		}
		else
		{
			CoolDown();
		}
	}

	private bool IsCoolDownComplete()
	{
		return Time.time > m_nextReadyTime;
	}

	private void DoAbility()
	{
		AbilityReady();
		if (Input.GetButtonDown(m_abilityButtonAxisName))
		{
			ButtonTriggered();
		}
	}

	private void AbilityReady()
	{
		m_coolDownTextDisplay.enabled = false;
		m_darkMask.enabled = false;
	}

	private void ButtonTriggered()
	{
		m_nextReadyTime = m_coolDownDuration + Time.time;
		m_coolDownTimeLeft = m_coolDownDuration;
		m_coolDownTextDisplay.enabled = true;
		m_darkMask.enabled = true;
		m_abilitySource.clip = m_ability.m_aSound;
		m_abilitySource.Play();
		m_ability.TriggerAbility();
	}

	private void CoolDown()
	{
		m_coolDownTimeLeft -= Time.deltaTime;
		float roundedCoolDown = Mathf.Round(m_coolDownTimeLeft);
		m_coolDownTextDisplay.text = roundedCoolDown.ToString();
		m_darkMask.fillAmount = (m_coolDownTimeLeft / m_coolDownDuration);
	}
}
