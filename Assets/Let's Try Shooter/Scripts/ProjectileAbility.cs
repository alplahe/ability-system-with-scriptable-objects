﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Abilities/ProjectileAbility")]
public class ProjectileAbility : Ability
{
	public float m_projectileForce = 250f;
	public Rigidbody m_projectile;

	private ProjectileShootTriggerable m_launcher;

	public override void Initialize(GameObject gameObject)
	{
		m_launcher = gameObject.GetComponent<ProjectileShootTriggerable>();
		m_launcher.m_projectileForce = m_projectileForce;
		m_launcher.m_projectile = m_projectile;
	}

	public override void TriggerAbility()
	{
		m_launcher.Launch();
	}
}
