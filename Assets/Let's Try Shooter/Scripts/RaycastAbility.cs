﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Abilities/RaycastAbility")]
public class RaycastAbility : Ability
{
	public int m_gunDamage = 1;
	public float m_weaponRange = 50f;
	public float m_hitForce = 100f;
	public Color m_laserColor = Color.white;

	private RaycastShootTriggerable m_raycastShootTriggerable;
	
	public override void Initialize(GameObject gameObject)
	{
		m_raycastShootTriggerable = gameObject.GetComponent<RaycastShootTriggerable>();
		m_raycastShootTriggerable.Initialize();

		m_raycastShootTriggerable.m_gunDamage = m_gunDamage;
		m_raycastShootTriggerable.m_weaponRange = m_weaponRange;
		m_raycastShootTriggerable.m_hitForce = m_hitForce;
		m_raycastShootTriggerable.m_laserLine.material = new Material(Shader.Find("Unlit/Color"));
		m_raycastShootTriggerable.m_laserLine.material.color = m_laserColor;
	}

	public override void TriggerAbility()
	{
		m_raycastShootTriggerable.Fire();
	}
}
